<!DOCKTYPE html>
<head>
    <meta charset=UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <title>TEST</title>
</head>
    <body>
        <div class="header">
            <div class="logo"><a href="http://test.ru">TEST</a></div>
            <div class="slogan">ERRORS ONLY!</div>
        </div>
        <div>
            <h1>HOBOSTY</h1>
        </div>

        <?php
        /**
         * Test
         * PHP version 5
         *
         * @category MyCategory
         * @package  MyPackage
         * @author   Kirill <kiryall@yandex.ru>
         * @license  https://kirill.ru PHP License
         * @link     https://kirill.ru
         */
        require 'incl/logs.php';
        require 'incl/db.php';
        $result_news = mysqli_query($connection, "SELECT * FROM `news`");
        ?>

            <div>

                <?php
                while (($news = mysqli_fetch_assoc($result_news))) {
                    $author_id = $news['author_id'];
                    ?>

                    <div>
                        <h3>
                            <a href="../text.php?id=<?php echo $news['id']; ?>">
                            <?php echo $news['title']; ?>
                            </a>
                        </h3>
                        </div>
                        <div>
                        <p style="color: #666666">
                            <?php
                            $date_ = date("d.m.y", $news['publish_date']);
                            echo $date_; ?>
                            </p>
                        </div>

                        <?php
                        echo mb_substr($news['text'], 0, 150, 'UTF-8') .
                        ' ...' . '<hr>';
                }
                ?>

            </div>

        <?php
        mysqli_close($connection);
        ?>

        <div class="footer">Все права <s> не </s> защищены, 2017-2017</div>
    </body>
