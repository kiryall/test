<?php

/**
 * Test
 * PHP version 5
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Kirill <kiryall@yandex.ru>
 * @license  https://kirill.ru PHP License
 * @link     https://kirill.ru
 */
require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$logger = new Logger('logger');

$debugHandler = new StreamHandler('logs/debug.log', Logger::DEBUG);
$warningHandler = new StreamHandler('logs/warning.log', Logger::WARNING);
$logHandler = new StreamHandler('logs/log.log');

$logger->pushHandler($debugHandler);
$logger->pushHandler($warningHandler);
$logger->pushHandler($logHandler);

$logger->debug('Отладочная информация');
$logger->warning('Предупреждение');
$logger->log(Error, 'Какой-то лог');

