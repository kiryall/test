<!DOCKTYPE html>
<head>
    <meta charset=UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <title>TEST</title>
</head>
    <body>
        <div class="header">
            <div class="logo"><a href="http://test.ru">TEST</a></div>
            <div class="slogan">ERRORS ONLY!</div>
        </div>
        <div>
            <h1>HOBOSTY</h1>
        </div>

        <?php
        /**
         * Test
         * PHP version 5
         *
         * @category MyCategory
         * @package  MyPackage
         * @author   Kirill <kiryall@yandex.ru>
         * @license  https://kirill.ru PHP License
         * @link     https://kirill.ru
         */
        require 'incl/logs.php';
        require 'incl/db.php';
        $authors_q = mysqli_query($connection, "SELECT * FROM `authors`");
        $authors = array();
        while ($aut = mysqli_fetch_assoc($authors_q)) {
            $authors[] = $aut;
        }
        ?>

        <div style="font-size: 25px;">

            <?php
            $author_name = false;
            foreach ($authors as $auth_) {
                if ($auth_['id'] == $_GET['id']) {
                    $author_name = $auth_;
                    break;
                }
            }
            echo $author_name['name'];
            ?>

        </div>
        <p style="color: #666666">Все статьи автора:</p>

        <?php
        $result_news = mysqli_query(
            $connection,
            "SELECT * FROM `news` WHERE `author_id`=" . $_GET['id']
        );
        while ($news = mysqli_fetch_assoc($result_news)) {
            ?>
            <h3>
                <a href="/text.php?id=<?php echo $news['id'];
                $news['title']; ?>
                </a>
            </h3>
            <?php
        }
        ?>

        <div class="footer">Все права <s> не </s> защищены, 2017-2017</div>
    </body>

