<!DOCKTYPE html>
<head>
    <meta charset=UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <title>TEST</title>
</head>
    <body>
        <div class="header">
            <div class="logo"><a href="http://test.ru">TEST</a></div>
            <div class="slogan">ERRORS ONLY!</div>
        </div>
        <div>
            <h1>HOBOSTY</h1>
        </div>

        <?php
        /**
         * Test
         * PHP version 5
         *
         * @category MyCategory
         * @package  MyPackage
         * @author   Kirill <kiryall@yandex.ru>
         * @license  https://kirill.ru PHP License
         * @link     https://kirill.ru
         */
        require 'incl/logs.php';
        require 'incl/db.php';
        $authors_q = mysqli_query($connection, "SELECT * FROM `authors`");
        $authors = [];
        while ($author_ = mysqli_fetch_assoc($authors_q)) {
            $authors[] = $author_;
        }
        $result_news = mysqli_query(
            $connection,
            "SELECT * FROM `news` WHERE `id` =" . $_GET['id']
        );
        $news = mysqli_fetch_assoc($result_news);
        ?>

            <div>
                <h3><?php echo $news['title']; ?></h3>
                <div>
                  <p style="color: #666666">

                        <?php
                        $date_ = date("d.m.y", $news['publish_date']);
                        echo $date_; ?>

                  </p>
                </div>
                <div>

                    <?php
                    $author_name = false;
                    foreach ($authors as $auth_) {
                        if ($auth_['id'] == $news['author_id']) {
                            $author_name = $auth_;
                            break;
                        }
                    }
                    echo 'Автор - ';
                    ?>

                    <a href="/author.php?id=<?php echo $author_name['id']; ?>">
                        <?php echo $author_name['name']; ?>
                    </a>
                </div>
                <p align="justify">    <?php echo $news['text']; ?> </p>
            </div>
        <div class="footer">Все права <s> не </s> защищены, 2017-2017</div>
    </body>